﻿using System;

// ReSharper disable InconsistentNaming
#pragma warning disable SA1611

namespace InsertionSort
{
    public static class Sorter
    {
        public static void InsertionSort(this int[] array)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            int keyElement, j;

            for (int i = 1; i < array.Length; i++)
            {
                keyElement = array[i];

                for (j = i - 1; j >= 0 && array[j] > keyElement; j--)
                {
                    array[j + 1] = array[j];
                }

                array[j + 1] = keyElement;
            }
        }

        public static void RecursiveInsertionSort(this int[] array)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            Sorting(array, array.Length);
        }

        public static void Sorting(int[] array, int n)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            if (n <= 1)
            {
                return;
            }

            Sorting(array, n - 1);

            int keyElement = array[n - 1], j;

            for (j = n - 2; j >= 0 && array[j] > keyElement; j--)
            {
                array[j + 1] = array[j];
            }

            array[j + 1] = keyElement;
        }
    }
}
